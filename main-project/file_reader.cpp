﻿

#include <iostream>﻿
#include <fstream>
#include <cstring>

using namespace std;

#include "constants.h"
#include "file_reader.h"
void read(const char* file_name, Chernota* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            Chernota* item = new Chernota;
            file >> item->day;

            file >> item->month;

            file >> item->number;
            file.getline(item->type, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "Ошибка";
    }
}
