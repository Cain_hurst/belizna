#ifndef FILE_READER_H
#define FILE_READER_H

#include "Chernota.h"

void read(const char* file_name, Chernota* array[], int& size);

#endif

