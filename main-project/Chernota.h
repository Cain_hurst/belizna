#ifndef CHERNOTA_H
#define CHERNOTA_H
#include "constants.h"

struct Chernota
{
    int day;
    int month;
    double number;
    char type[MAX_STRING_SIZE];

};

#endif

